import java.lang.NumberFormatException
val table = arrayOf(charArrayOf('0', '1', '2', '3'),
    charArrayOf('1', '-', '-', '-'),
    charArrayOf('2', '-', '-', '-'),
    charArrayOf('3', '-', '-', '-'))
var currentPlayer = 'X'
var turnCount = 0

fun main() {
    sayWelcome()
    while (true){
        printTable()
        showTurn()
        input()
        if (checkWinner() || turnCount == 8){
            break
        }
        switchPlayer()
    }
    printTable()
    showWinner()

}

private fun checkWinner(): Boolean {
    if (checkRow(1) || checkRow(2) || checkRow(3)) {
        return true
    }else if (checkCol(1) || checkCol(2) || checkCol(3)) {
        return true
    }else if (checkLeftDiagonal() || checkRightDiagonal()) {
        return true
    }
    return false
}

private fun showWinner() {
    if (turnCount == 8) {
        println("Draw!!!")
    }else {
        println(currentPlayer + " is Winner!!!")
    }
}

private fun checkRow(x: Int): Boolean {
    for (row in table) {
        if (table[x][1].equals(currentPlayer) && table[x][2].equals(currentPlayer) &&table[x][3].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkCol(x: Int): Boolean {
    for (row in table) {
        if (table[1][x].equals(currentPlayer) && table[2][x].equals(currentPlayer) &&table[3][x].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkLeftDiagonal(): Boolean {
    for (row in table) {
        if (table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer) && table[3][3].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkRightDiagonal(): Boolean {
    for (row in table) {
        if (table[1][3].equals(currentPlayer) && table[2][2].equals(currentPlayer) && table[3][1].equals(currentPlayer))
            return true
    }
    return false
}

private fun sayWelcome() {
    println("Welcome to game OX by Kotlin!")
}

private fun showTurn() {
    println(currentPlayer + " Turn!")
}

private fun switchPlayer() {
    if (currentPlayer == 'X') {
        currentPlayer = 'O'
    } else {
        currentPlayer = 'X'
    }
    turnCount += 1
}

private fun input() {
    while (true) {
        try {
            print("Please input (row,col): ")
            val input = readLine()
            val numStr = input?.split(" ")
            if (numStr?.size == 2) {
                val row = numStr?.get(0)?.toInt()
                val col = numStr?.get(1)?.toInt()
                if (table[row][col] != 'X' && table[row][col] != 'O') {
                    table[row][col] = currentPlayer
                }else {
                    println("Is not empty!")
                    continue
                }
                break
            } else {
                println("Please input again! (examplew : 1 1)")
            }
        } catch (e: NumberFormatException) {
            println("Please input again!")
        }
    }
}

private fun printTable() {
    for (row in table) {
        for (col in row) {
            print(col)
        }
        println()
    }
}
